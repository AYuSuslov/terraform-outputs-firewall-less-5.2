output "List_of_Hostnames" {
  value = "${google_compute_instance.vm_instance.*.name}"
}

output "GCP_Project_ID" {
  value = google_compute_instance.vm_instance.0.project
}
