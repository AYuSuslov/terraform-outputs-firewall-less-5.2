resource "google_compute_firewall" "http" {
  name    = "http-s-for-tf-nginx"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["80", "433"]
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["terraform-servers"]
}

resource "google_compute_firewall" "udp_host" {
  name    = "udp-for-tf-nginx"
  network = "default"
  allow {
    protocol = "udp"
    ports    = ["10000-20000"]
  }
  source_ranges = ["10.0.0.23"]
  target_tags = ["terraform-servers"]
}
