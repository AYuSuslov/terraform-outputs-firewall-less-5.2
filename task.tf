variable "node_count" {
  default    = "3"
}

resource "null_resource" "command_delete_hostfile" {
  provisioner "local-exec" {
    command = "rm -f host.list"
  }
}

resource "google_compute_instance" "vm_instance" {
  count        = "${var.node_count}"
  name         = "node${count.index+1}"
  machine_type = "e2-small"
  tags         = ["terraform-servers"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
      size  = "20"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} >> host.list"
  }

  connection {
    user = "suslov_ubuntu"
    timeout = "2m"
    host = "${self.network_interface.0.access_config.0.nat_ip}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get -y update",
      "sudo apt-get -y install nginx",
      "echo Juneway ${self.network_interface.0.access_config.0.nat_ip} ${self.name} | sudo tee /var/www/html/index.html",
      "sudo service nginx start",
    ]
  }
}
